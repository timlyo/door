# Everything relating to the physical door

import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)

def is_open() -> bool:
	return GPIO.input(18) == 0
