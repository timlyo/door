# Runs in the background and checks the state of the door, saving the information to a local redis server if changed

from doorman import door
import redis
import time
from datetime import datetime, tzinfo

r = redis.StrictRedis(host="localhost", port=6379, db=0)

def main():
	was_open = door.is_open()

	while True:
		if not door.is_open():
			if was_open # State has changed
				save_time_to_redis()
		was_open = door.is_open()
		time.sleep(1)

def save_time_to_redis():
	time = datetime.now()
	r.set("lock_time", time)

if __name__ == "__main__":
	main()
