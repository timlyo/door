#!/usr/bin/bash

echo "Installing service file"
cp doorman.service /usr/lib/systemd/system/
cp door_checker.service /usr/lib/systemd/system/

echo "Installing files"
mkdir -p /usr/share/doorman
cp main.py /usr/share/doorman
cp door_checker.py /usr/share/doorman
cp -r doorman /usr/share/doorman
cp -r lib /usr/share/doorman
cp -r static /usr/share/doorman
cp -r templates /usr/share/doorman


systemctl daemon-reload
