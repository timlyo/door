from flask import Flask, render_template, jsonify, send_from_directory
import redis
import logging

from doorman import door

app = Flask(__name__)

def get_redis():
    return redis.StrictRedis(host="localhost", port=6379, db=0)

@app.route("/")
def page():
    state = door.is_open()
    lock_time = get_redis().get("lock_time")
    app.logger.info(lock_time)
    return render_template("index.html", open=state, lock_time=str(lock_time))

@app.route("/state")
def check_open():
    return jsonify({
        "downstairs_bathroom_open": door.is_open()
        })

@app.route("/lib/<name>")
def get_lib(name):
    print("loading:", name)
    return send_from_directory("lib", name)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80, debug=True)
