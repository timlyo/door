function get_door_state() {
	$.get("/state", function(data) {
		on_door_update(data.downstairs_bathroom_open);
	})
}

function on_door_update(is_open) {
	if (door_open === null) {
		door_open = is_open;
	}

	// if state has changed
	if (door_open !== is_open) {
		if (is_open) {
			display_notification("Bathroom is free\n" + get_time_string());
		} else {
			display_notification("Bathroom is used\n" + get_time_string());
		}

		window.location.reload();
	}
}
