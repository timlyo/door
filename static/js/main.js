var door_open = null;

function main() {
	get_door_state();
	window.setInterval(get_door_state, 5000);
}

function display_notification(message) {
	if (!("Notification" in window)) {
		return;
	}

	try{
		if (Notification.permission === "granted") {
			var notification = new Notification(message);
		} else if (Notification.permission !== 'denied') {
			Notification.requestPermission(function(permission) {
				if (permission === "granted") {
					display_notification(message);
				}
			});
		}
	} catch (e) {
		console.log(e);
	}
}

function get_time_string(){
	var time = new Date()
	return time.toLocaleTimeString("en-GB")
}

window.onload = main;
